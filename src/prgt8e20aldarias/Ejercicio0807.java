/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt8e20aldarias;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Fichero: Ejercicio0807.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-feb-2014
 */
public class Ejercicio0807 {

  public static void seguir() {
    //espera la pulsacion de una tecla y luego RETORNO
    try {
      System.out.println("Pulsar tecla para seguir ...");
      System.in.read();
    } catch (Exception e) {
    }
  }

  public static void main(String[] args) {
    double[] precios = {1350.6, 400.7, 890.78, 6200.9, 8730.23};
    int[] unidades = {5, 7, 12, 8, 30};
    String[] descripciones = {"paquetes de papel",
      "lapices", "boligrafos",
      "carteras", "mesas"
    };
    // escribe los datos
    try {
      DataOutputStream salida = new DataOutputStream(
              new FileOutputStream("pedido.bin"));
      for (int i = 0; i < precios.length; i++) {
        salida.writeUTF(descripciones[i]);
        salida.writeInt(unidades[i]);
        salida.writeDouble(precios[i]);
      }
      salida.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    //leer los datos del archivo
    double precio;
    int unidad;
    String descripcion;
    double total = 0.0;
    try { // try1
      DataInputStream entrada =
              new DataInputStream(new FileInputStream("pedido.bin"));
      try { // try2
        while (true) {
          descripcion = entrada.readUTF();
          unidad = entrada.readInt();
          precio = entrada.readDouble();
          System.out.println("Has pedido " + unidad + " "
                  + descripcion + " a " + precio + " euros.");
          total = total + unidad * precio;
        }
      } catch (EOFException e) {
        System.out.println("por un TOTAL de: " + total + " euros.");
      } finally {
        if (entrada != null) {
          entrada.close();
        }
      } // fin try1
    } catch (FileNotFoundException e) { // try2
      System.err.println("FileNotFoundException:");
      e.printStackTrace();
    } catch (IOException e) {
      System.err.println("IOException");
      e.printStackTrace();
    } // fin try2
    seguir();
  }
}
/* EJECUCION:
 Has pedido 5 paquetes de papel a 1350.6 euros.
 Has pedido 7 lapices a 400.7 euros.
 Has pedido 12 boligrafos a 890.78 euros.
 Has pedido 8 carteras a 6200.9 euros.
 Has pedido 30 mesas a 8730.23 euros.
 por un TOTAL de: 331761.36 euros.
 */
